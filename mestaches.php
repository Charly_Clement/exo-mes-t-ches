<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    
    <title>Mes Tâches</title>
</head>

<body>
    <div class="container">
        <div class="jumbotron">
            <h1 class="display-4">Mes tâches</h1>
            <p class="lead">Un petit gestionnaire de tâches simple en PHP et sans base de données.</p>
        </div>
        <form action="mestaches.php" method="get">
            <div class="form-group">
                <label for="text">Ajouter une tâche</label>
                <input type="text" class="form-control" name="text" id="text">
            </div>
            <button type="submit" class="btn btn-primary">Ajouter la tâche</button>
        </form>

        <h3 class="my-3">Liste des tâches (<?php echo count(glob('task/text'))?>)</h3>

        <div class="alert alert-success" role="alert">Tâche créée</div>
        <div class="alert alert-danger" role="alert">Tâche supprimée</div>
        <?php
            if (isset($_GET['sup']) && is_file($_GET['sup'])) {
                unlink($_GET['sup']);
            }

            $recup = $_GET['text'];
            $nombre = time();

            if ($recup != "") {
                $filename = fopen("task/text".$nombre.".txt", "w");
                fwrite ($filename, $recup);
                fclose($filename);
            }

            echo "<div class='row'";

                foreach (glob('task/*.txt') as $fichier) {
                    $handle = fopen($fichier,'r');
                    $contents = fread($handle, filesize($fichier));
                    fclose($handle);
                    echo
                    "<div class='col-4'>
                        <div class='card my-3' style='width:18rem; justify-content:space-around;'>
                            <div class='card-body'>
                                <p class='card-text'> $contents </p>
                                <a href='mestaches.php?sup=". $fichier."'class='btn btn-warning'>Supprimer la tâche</a>
                            </div>
                        </div>
                    </div>";
                }
               
            echo "</div>";


        ?>

</body>
</html>